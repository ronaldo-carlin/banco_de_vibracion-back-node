// app.js
const express = require("express");
const cors = require("cors");
const app = express();
const swaggerUI = require("swagger-ui-express");
const openApiConfiguration = require("./docs/swagger");
const morganBody = require("morgan-body");
const customHeader = require("./middleware/customHeader");
const { dbConnectPostgresSql } = require("./database/postgres");
const path = require('path');

if (process.env.NODE_ENV === 'production') {
    require('dotenv').config({ path: 'production.env' });
} else {
    require('dotenv').config({ path: '.env.dev' });
}

app.use('/api/storage/users', express.static(path.join(__dirname, 'storage/users')));
app.use('/api/storage/logos', express.static(path.join(__dirname, 'storage/logos')));
app.use(cors());
app.use(express.json());
dbConnectPostgresSql();
morganBody(app, {
    noColors: true,
    logIP: true,
    skip: function (req, res) {
        return res.statusCode < 400;
    }
});
const port = process.env.PORT || 3124;
app.use("/documentation", swaggerUI.serve, swaggerUI.setup(openApiConfiguration));
app.get("/", (req, res) => {
    res.json({ message: "Server Up." });
});
app.use("/api", require("./routes"));
module.exports = app;
