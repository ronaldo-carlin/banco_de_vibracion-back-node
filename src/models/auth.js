const { Sequelize } = require("sequelize");
const { sequelize } = require("../database/postgres");
/* const ReferralModules = require("./referrals");
const LevelsModules = require("./levels"); */

const AuthModel = sequelize.define(
  "users",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: {
      type: Sequelize.TEXT,
    },
    last_name: {
      type: Sequelize.TEXT,
    },
    role_id: {
      type: Sequelize.INTEGER,
    },
    email: {
      type: Sequelize.TEXT,
    },
    password: {
      type: Sequelize.TEXT,
    },
    nickname: {
      type: Sequelize.TEXT,
    },
    image_path: {
      type: Sequelize.TEXT,
    },
    status: {
      type: Sequelize.INTEGER,
    },
    phone_number: {
      type: Sequelize.INTEGER,
    },
    last_login: {
      type: Sequelize.DATE,
    },
    guid: {
      type: Sequelize.TEXT,
    },
    city: {
      type: Sequelize.TEXT,
    },
    state: {
      type: Sequelize.TEXT,
    },
    created_by: {
      type: Sequelize.INTEGER,
    },
    updated_by: {
      type: Sequelize.INTEGER,
    }
  },
  {
    tableName: "users", // Nombre de la tabla en la base de datos
    timestamps: false, // Esto habilita automáticamente createdAt y updatedAt
    /* schema: "adm", */
  }
);
/* AuthModel.hasMany(ReferralModules, { foreignKey: "user_id", as: "referrals" });
AuthModel.belongsTo(LevelsModules, { foreignKey: "level_id", as: "level" }); */

/* AuthModel.belongsTo(RolesModel, {
  foreignKey: "IdRol",
  as: "Rol",
}); */

module.exports = AuthModel;
